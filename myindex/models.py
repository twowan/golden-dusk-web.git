from django.db import models
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField

# Create your models here.
class SecondaryClassification(models.Model):
    title = models.CharField(max_length=60,verbose_name="二级分类") #暂时不设计一级分类
    create_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = '二级分类'
        verbose_name_plural = '二级分类'


class TopicsOfSecondaryClassification(models.Model):
    secondary_classification_pid = models.ForeignKey(SecondaryClassification,on_delete=models.CASCADE, verbose_name="二级分类")
    title = models.CharField(max_length=255,verbose_name="主题")
    titile_content = RichTextUploadingField(verbose_name="主题内容",extra_plugins=['codesnippet'])
    create_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = '二级分类主题'
        verbose_name_plural = '二级分类主题'