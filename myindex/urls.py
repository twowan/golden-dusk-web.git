from django.urls import path
from .views import index,topicsOfSecondaryClassificationDetails,topicsOfSecondaryClassificationList
urlpatterns = [
    path(r'index/',index),
    path(r'topdetails/',topicsOfSecondaryClassificationDetails),
    path(r'toplist/',topicsOfSecondaryClassificationList),

]