from django.shortcuts import render,render_to_response,HttpResponse
from .models import SecondaryClassification,TopicsOfSecondaryClassification
# Create your views here.

def secondaryClassificationGlobal(req):
    sc = SecondaryClassification.objects.all()
    req.sc = sc
    return req

def index(req):
    secondaryClassificationGlobal(req)
    tosc = TopicsOfSecondaryClassification.objects.all()
    req.tosc=tosc
    return render(req,"index.html")

def topicsOfSecondaryClassificationDetails(req):
    secondaryClassificationGlobal(req)
    pk = req.GET.get("pk",None)
    content = TopicsOfSecondaryClassification.objects.get(pk=pk)
    req.topContent = content
    return render(req,"topicsOfSecondaryClassificationDetails.html")

def topicsOfSecondaryClassificationList(req):
    secondaryClassificationGlobal(req)
    pk = req.GET.get("pk",None)
    content = TopicsOfSecondaryClassification.objects.filter(secondary_classification_pid=pk)
    req.topContentList = content
    return render(req,"topicsOfSecondaryClassificationList.html")